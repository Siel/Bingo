defmodule Bingo.Buzzwords do
  @doc """
  Reads a CSV file of buzzwords and values

  returns a list of maps, each map containing the following keys:

    * phrase - The buzzword phrase.
    * points - The buzzword point value
  """
  # Path.expand converts a relative path to an absulute one
  # The trim: true opt in split removes all empty strings from the list

  def read_buzzwords do
    "../../data/buzzwords.csv"
    |> Path.expand(__DIR__)
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, ","))
    |> Enum.map(fn [phrase, points] -> %{phrase: phrase, points: String.to_integer(points)} end)
  end
end
