defmodule Bingo.GameSupervisor do
  use DynamicSupervisor

  alias Bingo.GameServer

  # Public interface

  def start_link(_args) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def start_game(game_name, size) do
    child_spec = %{
      id: GameServer,
      start: {GameServer, :start_link, [game_name, size]},
      restart: :transient
      #only restart processes that terminated abnormaly, timeout processes will
      #not be restarted
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  def stop_game(game_name) do
    :ets.delete(:games_table, game_name)
    child_pid = GameServer.game_pid(game_name)

    DynamicSupervisor.terminate_child(__MODULE__, child_pid)
  end

  def game_names do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.map(fn {_, game_pid, _, _} ->
      Registry.keys(Bingo.GameRegistry, game_pid) |> List.first()
    end)
    |> Enum.sort()
  end

  #Server Callbacks

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end


  # Private Functions
end
