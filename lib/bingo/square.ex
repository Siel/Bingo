defmodule Bingo.Square do
  @enforce_keys [:phrase, :points]
  defstruct phrase: nil, points: nil, marked_by: nil

  alias __MODULE__

  @doc """
  creates a new Square given phrase and points parameters
  """
  def new(phrase, points) do
    %Square{phrase: phrase, points: points}
  end

  @doc """
  creates a new square given a map with phrase and points keys
  """
  def from_buzzword(%{phrase: phrase, points: points}) do
    Square.new(phrase, points)
  end
end
